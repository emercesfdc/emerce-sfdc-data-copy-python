# sfdc data migration
This project is about a neat little python program that can read the data within an excel file by specific columns, transforms the data and uploads it to configurable sfdc instance

Additionally, with CCProductCopyScript you can copy cc product data (and all its related objects) from one sfdc org to another, based on a list of SKUs in an excel file.

CCProductCopyScript can also update product2 relationships on cc between source and target org. 

## Refactoring is very much needed
- Function name consistency
- Move bulk operations that are currently located inside SfdcService into the responsible sobject service, like bulk_insert_cc_products, that should be inside CCProductService instead of SfdcService
- order functions 
- order imports
- create Main Class for each executable py file (currentl Main.py and CCproductCopyScript.py)
    - add init methods to remove size of Main() function and call that init method
- any other refactorings you think make sense

## Dependencies

### Python & pip/pip3
- Make sure to have the latest version of python installed. Additionally pip / pip3 is needed for installation of further dependencies 

### OpenPYXL
```
pip3 install openpyxl
```

Not activeley used, required for Pandas

### Pandas
```
pip3 install pandas
```
https://pandas.pydata.org/docs/

### Simple-Salesforce
```
pip3 install simple-salesforce
```

https://pypi.org/project/simple-salesforce/#




