
class SfdcDmlOperationResponse:

    status = None
    message = None
    data = None
    errors = []
    error = False

    def set_error(self, status, errors, error):
        self.errors = errors
        self.error = error
        self.status = status

    def is_status_200(self):
        return self.status == 200

    def toJSON(self):
        return {
            "status": self.status,
            "message": self.message,
            "data": self.data,
            "error": self.error,
            "errors": self.errors
        }

    def get_size(self):
        return len(self.data)

    def get_size_as_string(self):
        return str(self.get_size())
