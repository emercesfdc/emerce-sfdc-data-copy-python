from constants.SObjectConstants import SObjectConstants


class Product2Constants(SObjectConstants):

    class ObjectFields:
        ID = "Id"
        NAME = "Name"
        IS_ACTIVE = "isActive"
        DESCRIPTION = "Description"
        MATERIAL_NUMBER = "Material_Number__c"
        WEIGHT_NET = "SMA_PR_NF_WeightNet__c"
        GROSS_WEIGHT = "SMA_PR_NF_GrossWeight__c"
        WIDTH_GROSS = "SMA_PR_NF_WidthGross__c"
        HEIGHT_GROSS = "SMA_PR_NF_HeightGross__c"
        DEPTH_GROSS = "SMA_PR_NF_DepthGross__c"
        UNIT_WEIGHT = "SMA_PR_PF_WeightUnit__c"
        UNIT_DIMENSION = "SMA_PR_PF_DimensionUnit__c"
        UNIT_VOLUME = "SMA_PR_PF_VolumeUnit__c"
        AVAILABLE_IN_SHOP = "isAvailableInShop__c"
    

    VALUE_UNIT_WEIGHT = "KGM"
    VALUE_UNIT_DIMENSION = "mm"
    VALUE_UNIT_VOLUME = "mm"
    VALUE_IS_AVAILABLE_IN_SHOP = True
    VALUE_IS_ACTIVE = True

    SOBJECT_API_NAME = "Product2"
