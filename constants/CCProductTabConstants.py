class CCProductTabConstants:

    class ObjectFields:
        TAB = "ccrz__Tab__c"
        PRODUCT = "ccrz__Product__c"
        PRODUCT_R = "ccrz__Product__r"
        PRODUCT_SKU = PRODUCT_R+".ccrz__SKU__c"
        CONTENT = "ccrz__ContentRT__c"
        ENABLED = "ccrz__Enabled__c"
        LOCALE = "ccrz__Locale__c"

    class Values:
        ENABLED = True
        CONTENT_PREFIX = "&lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;"
        CONTENT_SUFFIX = "w&quot; frameborder=&quot;0&quot; allow=&quot;autoplay; encrypted-media&quot; allowfullscreen&gt;&lt;/iframe&gt;"
        TAB = "Product Videos"

    SOBJECTNAME = "ccrz__E_ProductTab__c"