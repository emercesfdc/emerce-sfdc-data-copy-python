class CCProductLocaleConstants:

    class ObjectFields:
        NAME = "Name"
        PRODUCT = "ccrz__Product__c"
        PRODUCT_R = "ccrz__Product__r"
        PRODUCT_SKU = PRODUCT_R+".ccrz__SKU__c"
        SHORT_DESC = "ccrz__ShortDescRT__c"
        LONG_DESC = "ccrz__LongDescRT__c"
        LOCALE = "ccrz__Locale__c"


    SOBJECTNAME = "ccrz__E_ProductItemI18N__c"