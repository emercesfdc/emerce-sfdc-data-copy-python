class CCProductCategoryConstants:
    class ObjectFields:
        PRODUCT = "ccrz__Product__c"
        CATEGORY = "ccrz__Category__c"
    SOBJECTNAME = "ccrz__E_ProductCategory__c"