class ExcelServiceConstants:
    
    class ColumnKeys:
        SKU = "Material Number"
        NAME = "Name"
        SMA_PRODUCT_CLASSIFICATION = "SMA Product classification"
        PRODUCT_IMAGE = "Product Image"
        PRODUCT_SEARCH_IMAGE = "Product Search Image"
        PRODUCT_IMAGE_THUMBNAIL = "Product Image Thumbnail"
        STOREFRONT = "Storefront"
        VIDEO = "Video"
        DATASHEETS = "Datasheets"
        CATEGORY_ID = "Category ID"
        SHORT_DESC = "Short-Description"
        LONG_DESC = "Long-Description"
        PRICE = "Price"
        CC_PRICE_LIST_ID = "cc pricelist"
        LENGTH = "Length"
        WIDTH = "Width"
        HEIGHT = "Height"
        WEIGHT_NET = "Weight net (Product weight)"
        GROSS_WEIGHT = "Weight Gross (Shipping weight)"
        LOCALE = "Locale"

    class ColumnValues:
        SMA_PRODUCT_CLASSIFICATION_YES = "YES"
        SMA_PRODUCT_CLASSIFICATION_NO = "NO"
