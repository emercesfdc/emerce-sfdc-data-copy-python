from constants.SObjectConstants import SObjectConstants


class CCProductConstants(SObjectConstants):

    class ObjectFields:
        ID = "Id"
        NAME = "Name"

        CCRZ_PREFIX = "ccrz__"
        
        SKU = CCRZ_PREFIX + "SKU__c"
        SHORT_DESCRIPTION = CCRZ_PREFIX + "ShortDescRT__c"
        LONG_DESCRIPTION = CCRZ_PREFIX + "LongDescRT__c"
        STOREFRONTS = CCRZ_PREFIX + "Storefront__c"
        PRODUCT_STATUS = CCRZ_PREFIX + "ProductStatus__c"
        TAXABLE = CCRZ_PREFIX + "Taxable__c"
        SHIPPING_WEIGHT = CCRZ_PREFIX + "ShippingWeight__c"
        CLASSIFICATION = "Classification__c"

        EMERCE_MATERIAL_NUMBER = "emerceMaterialNumber__c"

        SMA_CC_PRODUCT = "sma_cc_Product__c"
        SMA_CC_PRODUCT_R = "sma_cc_Product__r"
        SMA_CC_PRODUCT_MATERIAL_NUMBER = SMA_CC_PRODUCT_R+".Material_Number__c"

    class ObjectValues:
        PRODUCT_STATUS = "Released"
        TAXABLE = True

    SOBJECT_API_NAME = 'ccrz__E_Product__c'
