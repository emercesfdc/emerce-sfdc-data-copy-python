class CCProductMediaConstants:

    class ObjectFields:
        CCPRODUCT = "ccrz__Product__c"
      

      
        ENABLED = "ccrz__Enabled__c"
        MEDIA_TYPE = "ccrz__MediaType__c"
        PRODUCT_MEDIA_SOURCE = "ccrz__ProductMediaSource__c"
        RECORDTYPE = "RecordType"
        RECORDTYPE_ID = "RecordTypeId"
        RECORDTYPE_NAME = "RecordType.Name"
        URI = "ccrz__URI__c"
        LOCALE = "ccrz__Locale__c"

        CCPRODUCT_R = "ccrz__Product__r"
        CCPRODUCT_SKU = CCPRODUCT_R+".ccrz__SKU__c"


    class FieldValues:
        PRODUCT_MEDIA_SOURCE = "URI"
        LOCALE = ""

    SOBJECTNAME = "ccrz__E_ProductMedia__c"
