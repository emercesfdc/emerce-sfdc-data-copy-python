from constants.SObjectConstants import SObjectConstants


class CCPriceListConstants(SObjectConstants):
    
    class ObjectFields:
        CCRZ_CURRENCYISOCODE = "ccrz__CurrencyISOCode__c"
        
    SOBJECTNAME = "ccrz__E_PriceList__c"
