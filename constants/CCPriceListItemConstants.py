class CCPriceListItemConstants:
    
    class ObjectFields:
        PRODUCT = "ccrz__Product__c"
        PRICE = "ccrz__Price__c"
        PRICE_LIST = "ccrz__PriceList__c"
    
    SOBJECTNAME = "ccrz__E_PriceListItem__c"