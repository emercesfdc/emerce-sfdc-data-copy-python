class SObjectConstants:

    ID = "Id"
    NAME = "Name"
    RECORDTYPE = "RecordType"
    RECORDTYPE_NAME = "RecordType.Name"
    RECORDTYPEID = "RecordTypeId"
    CURRENCY_ISO_CODE = "CurrencyIsoCode"
