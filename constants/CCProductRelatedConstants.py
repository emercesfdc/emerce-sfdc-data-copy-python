class CCProductRelatedConstants:

    class ObjectFields:
        CCPRODUCT = "ccrz__Product__c"
        CCPRODUCT_R = "ccrz__Product__r"
        CCPRODUCT_SKU = CCPRODUCT_R+".ccrz__SKU__c"
