class DependencyError(Exception):

    DEFAULT_MESSAGE = "Errors with Dependencies, at least one is null"

    def __init__(self, message, dependencies) -> None:
        super().__init__(message)
        self.dependencies = dependencies

    @classmethod
    def create(cls, dependencies):
        return DependencyError(
            cls.DEFAULT_MESSAGE,
            dependencies
        )

    def print_dependencies(self):
        print(self.dependencies)
