from constants.CCProductTabConstants import CCProductTabConstants
from builders.SoqlBuilder import SoqlBuilder
from models.CCProductMediaModel import CCProductMediaModel
from wrappers.IterableWrapper import IterableWrapper
from services.SObjectService import SObjectService

class CCProductTabService(SObjectService):

    def load_records_by_product_skus(self, skus, additional_fields = None) -> IterableWrapper:
        return self.sobjectmodel.load_records_by_product_skus(skus, additional_fields)

    def load_records_by_locale(self, locale):
        return IterableWrapper(
            self.sobjectmodel.load_records(
                where_conditions= [ 
                    SoqlBuilder.WhereCondition(
                        CCProductTabConstants.ObjectFields.LOCALE + " = {param}",
                        param=locale
                    )
                ] 
            )
        )
