from wrappers.IterableWrapper import IterableWrapper
from constants.ExcelServiceConstants import ExcelServiceConstants
import pandas as pd
from os import path


class ExcelService:

    constants = ExcelServiceConstants

    def __init__(self, dir, file) -> None:
        excel_file = pd.ExcelFile(path.join(dir, file))
        self.wb = pd.read_excel(excel_file)

    def set_index(self, indecies):
        self.wb.set_index(indecies, inplace=True)

    def get_values_by_column(self, columnName):
        return self.wb[columnName]

    def get_rows_by_column_and_values(self, column, keys) -> IterableWrapper:
        return IterableWrapper(
            self.wb.loc[
                self.wb[column].isin(keys)
            ].iterrows()
        )

    def get_row_by_index(self, index):
        return self.wb.loc[index]
