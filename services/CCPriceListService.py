from models.CCProductMediaModel import CCProductMediaModel
from wrappers.IterableWrapper import IterableWrapper
from services.SObjectService import SObjectService

class CCPriceListService(SObjectService):

    def load_cc_price_lists_by_names(self, names, additional_fields = None) -> IterableWrapper:
        return self.sobjectmodel.load_records_by_names(names, additional_fields)