from models.SObjectModel import SObjectModel
from wrappers.IterableWrapper import IterableWrapper


class SObjectService:

    def __init__(self, sobjectmodel:SObjectModel) -> None:
        self.sobjectmodel = sobjectmodel

    def find_missing_values_in_list_of_records(self, values, records, key) -> IterableWrapper:
        missing = []
        for value in values:
            found_value = False
            for record in records:
                if(record[key] == value):
                    found_value = True
                    break
            if(found_value == False):
                missing.append(value)
        return IterableWrapper(missing)

    def load_all(self, additional_fields = None):
        return self.sobjectmodel.load_records(
            additional_fields = additional_fields
        )