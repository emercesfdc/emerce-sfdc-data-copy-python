from services.CCProductTabService import CCProductTabService
from services.CCPriceListService import CCPriceListService
from models.CCProductTabModel import CCProductTabModel
from services.CCProductMediaService import CCProductMediaService
from models.CCProductMediaModel import CCProductMediaModel
from wrappers.CCProductListWrapper import CCProductListWrapper
from response_handlers.SfdcDmlOperationResponseHandler import SfdcDmlOperationResponseHandler
from constants.CCProductTabConstants import CCProductTabConstants
from constants.CCProductCategoryConstants import CCProductCategoryConstants
from constants.CCPriceListItemConstants import CCPriceListItemConstants
from exceptions.NoRecordTypeIdFoundException import NoRecordTypeIdFoundException
from constants.SObjectConstants import SObjectConstants
from constants.CCProductMediaConstants import CCProductMediaConstants
from response_handlers.CCProductDmlOperationResponseHandler import CCProductDmlOperationResponseHandler
from response_handlers.Product2DmlOperationResponseHandler import Product2DmlOperationResponseHandler
from responses.SfdcDmlOperationResponse import SfdcDmlOperationResponse
from constants.ExcelServiceConstants import ExcelServiceConstants
from wrappers.IterableWrapper import IterableWrapper
from wrappers.Product2ListWrapper import Product2ListWrapper
from utils.ListUtil import ListUtil
from models.CCProductModel import CCProductModel
from models.Product2Model import Product2Model
from models.CCPriceListModel import CCPriceListModel
from models.CCProductLocaleModel import CCProductLocaleModel
from constants.Product2Constants import Product2Constants
from constants.CCProductConstants import CCProductConstants
from services.Product2Service import ProductService
from services.CCProductService import CCProductService
from services.CCProductLocaleService import CCProductLocaleService
from simple_salesforce import exceptions
from connectors.SfdcConnector import SfdcConnector


class SfdcService:

    def __init__(self, sfdc_connector: SfdcConnector, excel_service) -> None:
        self.sfdc_connector = sfdc_connector

        self.product2_service = ProductService(
            Product2Model(sfdc_connector)
        )

        self.ccproduct_service = CCProductService(
            CCProductModel(sfdc_connector)
        )

        self.ccproduct_media_service = CCProductMediaService(
            CCProductMediaModel(sfdc_connector)
        )

        self.ccproduct_tab_service = CCProductTabService(
            CCProductTabModel(sfdc_connector)
        )

        self.ccprice_list_service = CCPriceListService(
            CCPriceListModel(sfdc_connector)
        )

        self.ccproduct_locale_service = CCProductLocaleService(
            CCProductLocaleModel(sfdc_connector)
        )

        self.excel_service = excel_service

        self.ccproduct_media_types = [
            ExcelServiceConstants.ColumnKeys.PRODUCT_IMAGE,
            ExcelServiceConstants.ColumnKeys.PRODUCT_SEARCH_IMAGE,
            ExcelServiceConstants.ColumnKeys.PRODUCT_IMAGE_THUMBNAIL,
            ExcelServiceConstants.ColumnKeys.DATASHEETS,
        ]

    def set_skus(self, skus):
        self.skus = skus

    def set_products(self, products):
        self.products = products

    def handle_product2(self, skus) -> Product2ListWrapper:

        self.set_skus(skus)

        self.set_products(
            self.product2_service.get_products_by_material_numbers(skus)
        )

        self.product2_skus_missing = self.product2_service.find_missing_values_in_list_of_records(
            skus,
            self.products.iterable
        )

        if(self.product2_skus_missing.is_not_empty_and_not_null()):

            excel_rows_of_missing_product2s = self.excel_service.get_rows_by_column_and_values(
                ExcelServiceConstants.ColumnKeys.SKU,
                self.product2_skus_missing.iterable
            )

            # excel_rows_of_missing_product2s_filtered = excel_rows_of_missing_product2s.filter_by_key_and_value(
            #     ExcelServiceConstants.ColumnKeys.SMA_PRODUCT_CLASSIFICATION,
            #     ExcelServiceConstants.ColumnValues.SMA_PRODUCT_CLASSIFICATION_NO
            # )

            product2_list = []
            for i, item in excel_rows_of_missing_product2s.iterable:
                product2_list.append(item)

            product2_json = self.prepare_product2_JSON(
                product2_list
            )

            product2_bulk_insert_response = self.bulk_insert_product2(
                product2_json
            )

            Product2DmlOperationResponseHandler(
                product2_bulk_insert_response).handle()

            self.set_products(
                self.product2_service.get_products_by_material_numbers(skus)
            )

        else:
            print("Info: All material numbers in excel file exist in SF already, proceeding now with handling of CC Products")

        return self.products

    def prepare_product2_JSON(self, rows):

        # insert new skus and add to existing records
        product2_json_list = []

        for row in rows:

            product2_json = {
                Product2Constants.ObjectFields.NAME: row.get(ExcelServiceConstants.ColumnKeys.NAME),
                Product2Constants.ObjectFields.MATERIAL_NUMBER: row.get(ExcelServiceConstants.ColumnKeys.SKU),
                Product2Constants.ObjectFields.IS_ACTIVE: Product2Constants.VALUE_IS_ACTIVE,
                Product2Constants.ObjectFields.WEIGHT_NET: row.get(ExcelServiceConstants.ColumnKeys.WEIGHT_NET),
                Product2Constants.ObjectFields.GROSS_WEIGHT: row.get(ExcelServiceConstants.ColumnKeys.GROSS_WEIGHT),
                Product2Constants.ObjectFields.WIDTH_GROSS: row.get(ExcelServiceConstants.ColumnKeys.WIDTH),
                Product2Constants.ObjectFields.HEIGHT_GROSS: row.get(ExcelServiceConstants.ColumnKeys.HEIGHT),
                Product2Constants.ObjectFields.DEPTH_GROSS: row.get(ExcelServiceConstants.ColumnKeys.LENGTH),
                Product2Constants.ObjectFields.UNIT_WEIGHT: Product2Constants.VALUE_UNIT_WEIGHT,
                Product2Constants.ObjectFields.UNIT_DIMENSION: Product2Constants.VALUE_UNIT_DIMENSION,
                Product2Constants.ObjectFields.UNIT_VOLUME: Product2Constants.VALUE_UNIT_VOLUME
            }
            product2_json_list.append(product2_json)

        return product2_json_list

    def is_sma_product(self, row):
        return str.lower(row.get(ExcelServiceConstants.ColumnKeys.SMA_PRODUCT_CLASSIFICATION)) == "yes"

    def bulk_operation(self, operation, records) -> SfdcDmlOperationResponse:
        response = SfdcDmlOperationResponse()

        try:
            data = operation(records)

            errors = IterableWrapper(
                self.get_errors(data)
            )

            if(errors.is_empty()):
                response.status = 200
            else:
                response.set_error(
                    500,
                    errors.iterable,
                    error=True
                )

            response.data = data

        except exceptions.SalesforceMalformedRequest as sfMalFormedRequestError:
            response.set_error(
                sfMalFormedRequestError.status,
                [sfMalFormedRequestError.message],
                error=True
            )
        except TypeError as typeErr:
            response.set_error(
                500,
                [typeErr],
                error=True
            )

        return response

    def bulk_insert_product2(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_product2_func(),
            records
        )

    def get_errors(self, data):
        errors = []

        for item in data:
            if(item.get('success') == False):
                errors.append(item.get('errors'))

        return errors

    def handle_ccproduct(self, skus, product2s) -> CCProductListWrapper:

        self.set_skus(skus)

        self.ccproducts = self.ccproduct_service.get_products_by_sku(skus)

        self.ccproduct_skus_missing = self.ccproduct_service.find_missing_values_in_list_of_records(
            skus,
            self.ccproducts.iterable
        )

        if(self.ccproduct_skus_missing.is_not_empty_and_not_null()):

            excel_rows_of_missing_ccproducts = self.excel_service.get_rows_by_column_and_values(
                ExcelServiceConstants.ColumnKeys.SKU,
                self.ccproduct_skus_missing.iterable
            )

            # excel_rows_of_missing_ccproducts_filtered = excel_rows_of_missing_ccproducts.filter_by_key_and_value(
            #     ExcelServiceConstants.ColumnKeys.SMA_PRODUCT_CLASSIFICATION,
            #     ExcelServiceConstants.ColumnValues.SMA_PRODUCT_CLASSIFICATION_NO
            # )

            ccproduct_list = []
            for i, item in excel_rows_of_missing_ccproducts.iterable:
                ccproduct_list.append(item)

            ccproduct_json = self.prepare_ccproduct_JSON(
                ccproduct_list
            )

            self.establish_product2_relationship(
                ccproduct_json,
                product2s
            )

            cc_product_insert_response = self.bulk_insert_ccproducts(
                ccproduct_json
            )

            CCProductDmlOperationResponseHandler(
                cc_product_insert_response
            ).handle()

            self.ccproducts = self.ccproduct_service.get_products_by_sku(skus)

        else:
            print("Info: All material numbers exist in SF as CC Products, proceeding now with CC Product - Related Data (Media, Datasheets, Videos, Categories)")

        return self.ccproducts

    def prepare_ccproduct_JSON(self, rows):
        ccproduct_json_list = []

        for row in rows:

            ccproduct_JSON = {
                CCProductConstants.ObjectFields.NAME: row.get(ExcelServiceConstants.ColumnKeys.NAME),
                CCProductConstants.ObjectFields.SKU: row.get(ExcelServiceConstants.ColumnKeys.SKU),
                CCProductConstants.ObjectFields.SHORT_DESCRIPTION: row.get(ExcelServiceConstants.ColumnKeys.SHORT_DESC),
                CCProductConstants.ObjectFields.LONG_DESCRIPTION: row.get(ExcelServiceConstants.ColumnKeys.LONG_DESC),
                CCProductConstants.ObjectFields.STOREFRONTS: row.get(ExcelServiceConstants.ColumnKeys.STOREFRONT),
                CCProductConstants.ObjectFields.PRODUCT_STATUS: CCProductConstants.ObjectValues.PRODUCT_STATUS,
                CCProductConstants.ObjectFields.TAXABLE: CCProductConstants.ObjectValues.TAXABLE,
                CCProductConstants.ObjectFields.EMERCE_MATERIAL_NUMBER: row.get(ExcelServiceConstants.ColumnKeys.SKU),
            }
            ccproduct_json_list.append(ccproduct_JSON)

        return ccproduct_json_list

    def establish_product2_relationship(self, ccproducts, product2s):
        for ccproduct in ccproducts:
            for product2 in product2s:
                if(ccproduct.get(CCProductConstants.ObjectFields.SKU) == product2.get(Product2Constants.ObjectFields.MATERIAL_NUMBER)):
                    self.set_product2_to_ccproduct(ccproduct, product2)
                    self.set_shipping_weight(ccproduct, product2)
                    break

    
    def link_product2s_with_ccproducts(self, ccproducts, product2s):
        for ccproduct in ccproducts:
            for product2 in product2s:
                if(ccproduct.get(CCProductConstants.ObjectFields.SKU) == product2.get(Product2Constants.ObjectFields.MATERIAL_NUMBER)):
                    self.set_product2_to_ccproduct(ccproduct, product2)

    def set_product2_to_ccproduct(self, ccproduct, product2):
        ccproduct[CCProductConstants.ObjectFields.SMA_CC_PRODUCT] = product2.get(
            Product2Constants.ObjectFields.ID
        )

    def set_shipping_weight(self, ccproduct, product2):
        ccproduct[CCProductConstants.ObjectFields.SHIPPING_WEIGHT] = self.calculate_shipping_weights(
            product2)

    def calculate_shipping_weights(self, product2):

        width = product2.get(Product2Constants.ObjectFields.WIDTH_GROSS)
        height = product2.get(Product2Constants.ObjectFields.HEIGHT_GROSS)
        depth = product2.get(Product2Constants.ObjectFields.DEPTH_GROSS)

        volumeWeight = height * depth * width
        volumeWeight = volumeWeight / (100000000*300)

        shippingWeight = product2.get(
            Product2Constants.ObjectFields.GROSS_WEIGHT)

        if(volumeWeight == None):
            print(
                "For", 
                product2.get(Product2Constants.ObjectFields.MATERIAL_NUMBER), 
                "no volume weight could be calculated. Skipping this product"
            )
            return 0

        if(shippingWeight == None):
            print(
                "For", 
                product2.get(Product2Constants.ObjectFields.MATERIAL_NUMBER), 
                "no shipping weight could be calculated. Skipping this product"
            )
            return 0
        
        if(volumeWeight > shippingWeight):
            shippingWeight = volumeWeight

        return shippingWeight

    def bulk_insert_ccproducts(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_ccproduct_func(),
            records
        )

    def bulk_update_ccproducts(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_update_ccproduct_func(), 
            records
        )

    def handle_cc_product_related_data(self, ccproducts):
        cc_product_medias = []
        cc_price_list_items = []
        cc_product_categories = []
        cc_product_tabs = []

        self.cc_product_media_recordtypes = self.load_product_media_recordtypes()

        self.product_media_image_recordtype_id = self.get_product_media_recordtype_id_by_name(
            "Media")
        self.product_media_datasheet_recordtype_id = self.get_product_media_recordtype_id_by_name(
            "Datasheet")

        if(self.product_media_image_recordtype_id == None):
            raise NoRecordTypeIdFoundException(
                message="No record type id found for 'Media'"
            )

        if(self.product_media_datasheet_recordtype_id == None):
            raise NoRecordTypeIdFoundException(
                message="No record type id found for 'Datasheet'"
            )

        for ccproduct in ccproducts:

            for media_type in self.ccproduct_media_types:
                product_media_json = self.create_cc_product_media_json(
                    ccproduct,
                    self.get_product_media_recordtype_id_by_media_type(media_type),
                    media_type
                )
                if(not product_media_json == None):
                    cc_product_medias.append(product_media_json)

            # cc_price_list_item_json = self.create_cc_price_list_item(ccproduct)
            # if(not cc_price_list_item_json == None):
            #     cc_price_list_items.append(cc_price_list_item_json)

            # cc_product_category_json = self.create_cc_product_category(
            #     ccproduct)
            # if(not cc_product_category_json == None):
            #     cc_product_categories.append(cc_product_category_json)

            # cc_product_tab_json = self.create_cc_product_tab(ccproduct)
            # if(not cc_product_tab_json == None):
            #     cc_product_tabs.append(cc_product_tab_json)

        if(len(cc_product_medias) > 0):
            cc_product_media_insert_response = self.bulk_insert_cc_product_media(
                cc_product_medias)
            SfdcDmlOperationResponseHandler(
                cc_product_media_insert_response, CCProductMediaConstants.SOBJECTNAME).handle() 

        if(len(cc_price_list_items) > 0):
            cc_price_list_item_insert_response = self.bulk_insert_cc_price_list_items(
                cc_price_list_items)
            SfdcDmlOperationResponseHandler(
                cc_price_list_item_insert_response, CCPriceListItemConstants.SOBJECTNAME).handle()

        if(len(cc_product_categories) > 0):
            cc_product_categories_insert_response = self.bulk_insert_cc_product_categories(
                cc_product_categories)
            SfdcDmlOperationResponseHandler(
                cc_product_categories_insert_response, CCProductCategoryConstants.SOBJECTNAME).handle()

        if(len(cc_product_tabs) > 0):
            cc_product_tab_insert_response = self.bulk_insert_cc_product_tabs(
                cc_product_tabs)
            SfdcDmlOperationResponseHandler(
                cc_product_tab_insert_response, CCProductTabConstants.SOBJECTNAME).handle()


    def get_product_media_recordtype_id_by_media_type(self, media_type:str):

        recordtype_id = None

        if "Image" in media_type:
            recordtype_id = self.get_product_media_recordtype_id_by_name(
            "Media")
        else:
            recordtype_id = self.get_product_media_recordtype_id_by_name(
            "Datasheet")

        return recordtype_id


    def bulk_insert_cc_product_media(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_product_media_func(),
            records
        )

    def bulk_insert_cc_price_list_items(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_price_list_items_func(),
            records
        )

    def bulk_insert_cc_product_categories(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_product_categoriesfunc(),
            records
        )

    def bulk_insert_cc_product_tabs(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_product_tab_func(),
            records
        )

    def load_product_media_recordtypes(self):
        return self.sfdc_connector.load_record_types_cc_product_media()

    def get_product_media_recordtype_id_by_name(self, name):
        record_type_id = None
        for record_type in self.cc_product_media_recordtypes:
            if(name == record_type.get(SObjectConstants.NAME)):
                record_type_id = record_type.get(SObjectConstants.ID)
                break
        return record_type_id

    def create_cc_product_media_json(self, ccproduct, recordtype_id, mediatype):

        cc_product_media_json = None

        excel_row = self.excel_service.get_row_by_index(
            ccproduct.get(CCProductConstants.ObjectFields.SKU)
        )

        image_url = excel_row.get(mediatype)

        if(type(image_url) == str):
            cc_product_media_json = {
                CCProductMediaConstants.ObjectFields.CCPRODUCT: ccproduct.get(CCProductConstants.ObjectFields.ID),
                CCProductMediaConstants.ObjectFields.RECORDTYPE_ID: recordtype_id,
                CCProductMediaConstants.ObjectFields.MEDIA_TYPE: mediatype,
                CCProductMediaConstants.ObjectFields.PRODUCT_MEDIA_SOURCE: CCProductMediaConstants.FieldValues.PRODUCT_MEDIA_SOURCE,
                CCProductMediaConstants.ObjectFields.URI: image_url,
                CCProductMediaConstants.ObjectFields.LOCALE: CCProductMediaConstants.FieldValues.LOCALE
            }

        return cc_product_media_json

    def create_cc_price_list_item(self, ccproduct):
        cc_price_list_item_json = None

        excel_row = self.excel_service.get_row_by_index(
            ccproduct.get(CCProductConstants.ObjectFields.SKU)
        )

        price_list_id = excel_row.get(
            ExcelServiceConstants.ColumnKeys.CC_PRICE_LIST_ID)

        if(type(price_list_id) == str):
            price = excel_row.get(ExcelServiceConstants.ColumnKeys.PRICE)
            if(type(price) == str):
                print("type of price is string, must be integer/float/double")
            else:
                cc_price_list_item_json = {
                    CCPriceListItemConstants.ObjectFields.PRICE: float(price),
                    CCPriceListItemConstants.ObjectFields.PRICE_LIST: price_list_id,
                    CCPriceListItemConstants.ObjectFields.PRODUCT: ccproduct.get(
                        SObjectConstants.ID)
                }

        return cc_price_list_item_json

    def create_cc_product_category(self, ccproduct):
        cc_product_category_json = None

        excel_row = self.excel_service.get_row_by_index(
            ccproduct.get(CCProductConstants.ObjectFields.SKU)
        )

        category_id = excel_row.get(
            ExcelServiceConstants.ColumnKeys.CATEGORY_ID
        )

        if(type(category_id) == str):
            cc_product_category_json = {
                CCProductCategoryConstants.ObjectFields.CATEGORY: category_id,
                CCPriceListItemConstants.ObjectFields.PRODUCT: ccproduct.get(
                    SObjectConstants.ID
                )
            }

        return cc_product_category_json

    def create_cc_product_tab(self, ccproduct):
        cc_product_tab_json = None

        excel_row = self.excel_service.get_row_by_index(
            ccproduct.get(CCProductConstants.ObjectFields.SKU)
        )

        video_url = excel_row.get(
            ExcelServiceConstants.ColumnKeys.VIDEO
        )

        locale = excel_row.get(
            ExcelServiceConstants.ColumnKeys.LOCALE
        )

        if(type(video_url) == str and type(locale) == str):
            cc_product_tab_json = {
                CCProductTabConstants.ObjectFields.TAB: CCProductTabConstants.Values.TAB,
                CCProductTabConstants.ObjectFields.PRODUCT: ccproduct.get(
                    SObjectConstants.ID
                ),
                CCProductTabConstants.ObjectFields.CONTENT: CCProductTabConstants.Values.CONTENT_PREFIX +
                video_url+CCProductTabConstants.Values.CONTENT_SUFFIX,
                CCProductTabConstants.ObjectFields.ENABLED: CCProductTabConstants.Values.ENABLED
            }

        return cc_product_tab_json

    def bulk_insert_cc_price_lists(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_price_list_func(),
            records
        ) 

    def bulk_insert_cc_product_locales(self, records) -> SfdcDmlOperationResponse:
        return self.bulk_operation(
            self.sfdc_connector.get_bulk_insert_cc_product_locale_func(),
            records
        )  

    