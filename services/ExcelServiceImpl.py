from services.ExcelService import ExcelService


class ExcelServiceImpl(ExcelService):

    def __init__(self, dir, file) -> None:
        super().__init__(dir, file)

    def get_values_by_sku(self):
        return self.get_values_by_column(
            self.constants.ColumnKeys.SKU
        )
    

