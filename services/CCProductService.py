from builders.SoqlBuilder import SoqlBuilder
from wrappers.IterableWrapper import IterableWrapper
from wrappers.CCProductListWrapper import CCProductListWrapper
from constants.CCProductConstants import CCProductConstants
from services.SObjectService import SObjectService


class CCProductService(SObjectService):

    def find_missing_values_in_list_of_records(self, skus, ccproducts) -> IterableWrapper:
        return super().find_missing_values_in_list_of_records(
            skus,
            ccproducts,
            CCProductConstants.ObjectFields.SKU
        )

    def get_products_by_sku(self, skus, additional_fields = None) -> CCProductListWrapper:
        return self.sobjectmodel.load_records_by_skus(skus, additional_fields=additional_fields)

    def get_all_products_with_product2(self, additional_fields = None) -> CCProductListWrapper:
        return self.sobjectmodel.load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    CCProductConstants.ObjectFields.SMA_CC_PRODUCT + " != null"
                )
            ],
            additional_fields = additional_fields
        )