from wrappers.Product2ListWrapper import Product2ListWrapper
from wrappers.IterableWrapper import IterableWrapper
from models.Product2Model import Product2Model
from constants.Product2Constants import Product2Constants
from services.SObjectService import SObjectService


class ProductService(SObjectService):

    def find_missing_values_in_list_of_records(self, skus, products) -> IterableWrapper:
        return super().find_missing_values_in_list_of_records(
            skus,
            products,
            Product2Constants.ObjectFields.MATERIAL_NUMBER
        )

    def get_products_by_material_numbers(self, skus, additional_fields = None) -> Product2ListWrapper:
        return self.sobjectmodel.load_records_by_skus(skus, additional_fields) 
