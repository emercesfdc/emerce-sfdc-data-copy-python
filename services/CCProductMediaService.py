from models.CCProductMediaModel import CCProductMediaModel
from wrappers.IterableWrapper import IterableWrapper
from services.SObjectService import SObjectService

class CCProductMediaService(SObjectService):

    def load_records_by_product_skus(self, skus, additional_fields = None) -> IterableWrapper:
        return self.sobjectmodel.load_records_by_product_skus(skus, additional_fields)