# @Author: Bastian Kaltdorf
# Created Date: 02.07.2021
# Last Modified Date: 25.07.2021
# Version: 0.1
# Description: Reads product related information from an Excel File, and tries to insert either new data, or update existing data in Salesforce
# Purpose: reduce required time, effort, and errors for seller onboarding

import time
from constants.ExcelServiceConstants import ExcelServiceConstants
from services.SfdcService import SfdcService
from connectors.SfdcConnector import SfdcConnector
from services.ExcelServiceImpl import ExcelServiceImpl
import configparser


def main():

    start_time = time.time()

    config = configparser.ConfigParser()

    config.read('_configs/config.ini')

    meta_config = config["META"]
    sfdc_config_to_use = meta_config["sfdc_config"]

    print("we are targeting configuration: ", sfdc_config_to_use, "\n\n")

    excel_config = config["EXCEL"]
    sfdc_config = config[sfdc_config_to_use]

    excel_service = ExcelServiceImpl(
        excel_config.get("dir"),
        excel_config.get("file")
    )

    excel_service.set_index(
        excel_service.wb[ExcelServiceConstants.ColumnKeys.SKU]
    )

    sfdc_service = SfdcService(
        SfdcConnector(
            username=sfdc_config["username"],
            password=sfdc_config["password"],
            security_token=sfdc_config["security_token"],
            domain=sfdc_config["domain"]
        ),
        excel_service
    )

    skus = excel_service.get_values_by_sku().tolist()

    product2_list_wrapper = sfdc_service.handle_product2(
        skus
    )

    ccproduct_list_wrapper = sfdc_service.handle_ccproduct(
        skus,
        product2_list_wrapper.iterable
    )

    if(ccproduct_list_wrapper.is_not_empty_and_not_null()):

        sfdc_service.handle_cc_product_related_data(
            ccproduct_list_wrapper.iterable
        )


    time_finish = "{:.2f}".format(time.time() - start_time)

    print ("Done")
    print("---execution time: %s seconds  ---" % time_finish)
    
main()

