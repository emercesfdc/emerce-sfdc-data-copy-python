from response_handlers.SfdcDmlOperationResponseHandler import SfdcDmlOperationResponseHandler
from responses.SfdcDmlOperationResponse import SfdcDmlOperationResponse


class Product2DmlOperationResponseHandler(SfdcDmlOperationResponseHandler):
    def __init__(self, response: SfdcDmlOperationResponse) -> None:
        super().__init__(response, sobject="Product2")
