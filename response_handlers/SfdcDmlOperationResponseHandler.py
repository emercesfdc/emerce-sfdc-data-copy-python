from responses.SfdcDmlOperationResponse import SfdcDmlOperationResponse


class SfdcDmlOperationResponseHandler:
    def __init__(self, response: SfdcDmlOperationResponse, sobject=None) -> None:
        self.response = response
        self.sobject = sobject

    def handle(self):
        if(self.response.error):
            print('Error processing data, check errors')
            print("Errors: ", self.response.errors)
            print("Object", self.sobject)

        if(not self.sobject == None and self.response.is_status_200()):
            print("Success: Processed "+self.response.get_size_as_string() +
                  " records for Object", self.sobject)
