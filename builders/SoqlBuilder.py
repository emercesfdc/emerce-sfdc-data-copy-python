from simple_salesforce import format_soql
from request_types.SoqlRequest import SoqlRequest


class SoqlBuilder:

    class WhereCondition:
        def __init__(self, query, param = None) -> None:
            self.query = query
            self.param = param

    @classmethod
    def create_soql_request(cls, obj, fields, where_conditions):
        base_query = cls.build_base_query(obj, fields)
        full_query = cls.build_full_query(base_query, where_conditions)

        return SoqlRequest(
            base_query,
            full_query
        )

    @classmethod
    def build_base_query(cls, obj, fields):
        return "SELECT "+cls.prepare_fields_for_query(fields)+" FROM "+obj

    @classmethod
    def prepare_fields_for_query(cls, fields):
        return ', '.join(fields)

    @classmethod
    def build_full_query(cls, base_query, where_conditions):
        full_query = base_query
        if(where_conditions != None and len(where_conditions) > 0):
            full_query += " WHERE "
            for where_condition in where_conditions:
                if(where_condition.param == None):
                    full_query += where_condition.query
                else:
                    full_query += format_soql(
                        where_condition.query,
                        param=where_condition.param
                    )
        return full_query
