


from builders.SoqlBuilder import SoqlBuilder


soqlRequest = SoqlBuilder .create_soql_request(
    "Product2",
    ["Id", "Name"],
    [
        SoqlBuilder.WhereCondition(
            "Name like {param}",
            "%Sunny%"
        ),
        SoqlBuilder.WhereCondition(
            " OR Name != null"
        ),
    ]
)

print(soqlRequest.base_query)
print(soqlRequest.full_query)
