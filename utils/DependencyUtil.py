from DependencyError import DependencyError


class DependencyUtil:

    @classmethod
    def dependencies_are_not_null(cls, dependencies):
        valid = True
        for dependency in dependencies:
            if dependency == None:
                valid = False
                break
        return valid

    @classmethod
    def validate_dependencies(cls, dependencies):
        if(cls.dependencies_are_not_null(dependencies) == False):
            raise DependencyError.create(
                dependencies
            )
