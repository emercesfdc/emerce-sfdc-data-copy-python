class ListUtil:

    @classmethod
    def is_not_null_and_not_empty(cls, list):
        return cls.is_not_null(list) and cls.is_not_empty(list)

    @classmethod
    def is_not_null(cls, list):
        return list != None

    @classmethod
    def is_not_empty(cls, list):
        return len(list) > 0