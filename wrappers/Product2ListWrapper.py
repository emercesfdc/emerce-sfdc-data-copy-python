from wrappers.SObjectListWrapper import SObjectListWrapper
from constants.Product2Constants import Product2Constants

class Product2ListWrapper(SObjectListWrapper):

    def find_product_by_material_number(self, material_number):
        return self.find_item_by_key_and_value(
            Product2Constants.ObjectFields.MATERIAL_NUMBER,
            material_number
        )