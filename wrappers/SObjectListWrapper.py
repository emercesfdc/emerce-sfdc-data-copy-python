
class SObjectListWrapper:

    def __init__(self, iterable) -> None:
        self.iterable = iterable

    def find_item_by_key_and_value(self, key, value):
        found_value = None
        for sobject in self.iterable:
            if(sobject[key] == value):
                found_value = value
                break
        return found_value

    def is_not_empty_and_not_null(self) -> bool:
        return not self.is_empty_or_null()

    def is_empty_or_null(self)->bool:
        return self.is_null() or self.is_empty()

    def is_null(self)->bool:
        return self.iterable == None
    
    def is_empty(self)->bool:
        return self.size() <= 0

    def size(self)->int:
        return len(self.iterable)

    def filter_by_key_and_value(self, k, v):
        filtered_iterable = []

        for i, item in self.iterable:
            item_v = item.get(k)
            if(item_v == v):
                filtered_iterable.append(item)
        
        return filtered_iterable

    def get_relationship_values(self, relationship_name, key):
        values = []

        for record in self.iterable:
            value = record.get(relationship_name).get(key)
            if(not value in values):
                values.append(value)
            
        return values

    def get_values_by_key(self, key):
        values = []

        for record in self.iterable:
            value = record[key]
            if(not value in values):
                values.append(value)
            
        return values

    def append(self, record):
        self.iterable.append(record)