from SObjectWrapper import SObjectWrapper
from Product2Constants import Product2Constants


class Product2Wrapper(SObjectWrapper):

    def __init__(self, product: dict) -> None:
        super().__init__(product)

    def get_volume(self) -> float:

        width_gross = self.__getattribute__(
            Product2Constants.ObjectFields.WIDTH_GROSS)

        height_gross = self.__getattribute__(
            Product2Constants.ObjectFields.HEIGHT_GROSS)

        depth_gross = self.__getattribute__(
            Product2Constants.ObjectFields.DEPTH_GROSS)

        volume = width_gross * height_gross * depth_gross

        return volume
