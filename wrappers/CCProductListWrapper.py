from wrappers.SObjectListWrapper import SObjectListWrapper
from constants.CCProductConstants import CCProductConstants


class CCProductListWrapper(SObjectListWrapper):

    def find_product_by_sku(self, sku):
        return self.find_item_by_key_and_value(
            CCProductConstants.ObjectFields.SKU,
            sku
        )
    
    def get_all_skus(self):
        return self.get_values_by_key(CCProductConstants.ObjectFields.SKU)

    def get_storefronts(self):
        storefronts = []
        for ccproduct in self.iterable:
            cc_product_storefronts = ccproduct.get(CCProductConstants.ObjectFields.STOREFRONTS)
            if(not cc_product_storefronts == None):
                for storefront in cc_product_storefronts.split(';'):
                    if(not storefront in storefronts):
                        storefronts.append(storefront)

        return storefronts