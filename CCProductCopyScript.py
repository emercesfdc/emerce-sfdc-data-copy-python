
import configparser
from constants.CCPriceListConstants import CCPriceListConstants
from wrappers.IterableWrapper import IterableWrapper
from wrappers.CCProductListWrapper import CCProductListWrapper

from models.CCProductTabModel import CCProductTabModel
from constants.CCProductRelatedConstants import CCProductRelatedConstants
from constants.CCProductMediaConstants import CCProductMediaConstants
from response_handlers.SfdcDmlOperationResponseHandler import SfdcDmlOperationResponseHandler
from response_handlers.Product2DmlOperationResponseHandler import Product2DmlOperationResponseHandler
from constants.SObjectConstants import SObjectConstants
from response_handlers.CCProductDmlOperationResponseHandler import CCProductDmlOperationResponseHandler
from constants.Product2Constants import Product2Constants
from constants.ExcelServiceConstants import ExcelServiceConstants
from services.ExcelServiceImpl import ExcelServiceImpl
from services.SfdcService import SfdcService
from connectors.SfdcConnector import SfdcConnector
from constants.CCProductConstants import CCProductConstants
from constants.CCProductTabConstants import CCProductTabConstants

import time


#init
def Main():

    start_time = time.time()

    config = configparser.ConfigParser()

    config.read('_configs/config.ini')

    sfdc_config_source = config[config["META"]["sfdc_copy_source"]]
    sfdc_config_target = config[config["META"]["sfdc_copy_target"]]


    excel_config = config["EXCEL_COPY"]

    excel_service = ExcelServiceImpl(
        excel_config.get("dir"),
        excel_config.get("file")
    )

    excel_service.set_index(
        excel_service.wb[ExcelServiceConstants.ColumnKeys.SKU]
    )

    sfdc_service_source = SfdcService(
        SfdcConnector(
            username=sfdc_config_source["username"],
            password=sfdc_config_source["password"],
            security_token=sfdc_config_source["security_token"],
            domain=sfdc_config_source["domain"]
        ),
        excel_service
        )

    sfdc_service_target = SfdcService(
        SfdcConnector(
            username=sfdc_config_target["username"],
            password=sfdc_config_target["password"],
            security_token=sfdc_config_target["security_token"],
            domain=sfdc_config_target["domain"]
        ),
        excel_service
        )

    skus = excel_service.get_values_by_sku().tolist()

    # copy_product2(skus, sfdc_service_source, sfdc_service_target)
    # new_cc_products = copy_cc_products(skus, sfdc_service_source, sfdc_service_target)

    # if(new_cc_products.is_not_empty_and_not_null()):
    #     copy_cc_product_media(sfdc_service_source, sfdc_service_target, new_cc_products)
    #     copy_cc_product_tabs(sfdc_service_source, sfdc_service_target, new_cc_products)

    # cc_price_lists = create_cc_price_lists(
    #     sfdc_service_target.ccproduct_service.get_products_by_sku(skus),
    #     sfdc_service_target
    # )

    # cc_price_list_items = create_cc_price_list_items(
    #     sfdc_service_target.ccproduct_service.get_products_by_sku(skus),
    #     cc_price_lists,
    #     sfdc_service_target
    # )

    # correct_product2_on_ccproducts(
    #     sfdc_service_source,
    #     sfdc_service_target
    # )

    # copy_cc_product_tabs_by_skus(skus, sfdc_service_source, sfdc_service_target)

    copy_cc_product_tabs_from_locale_to_another_locale("en_US", "hu_HU", sfdc_service_target)

    print("---execution time: %s seconds  ---" % (time.time() - start_time))

def correct_product2_on_ccproducts(sfdc_service_source:SfdcService, sfdc_service_target:SfdcService):

    additional_fields = [
        CCProductConstants.ObjectFields.SMA_CC_PRODUCT, 
        CCProductConstants.ObjectFields.SKU,
        CCProductConstants.ObjectFields.SMA_CC_PRODUCT_MATERIAL_NUMBER
    ]

    ccproducts_in_source = CCProductListWrapper(
        sfdc_service_source.ccproduct_service.get_all_products_with_product2(additional_fields)   
    ) 


    product2_material_numbers = ccproducts_in_source.get_relationship_values(
        CCProductConstants.ObjectFields.SMA_CC_PRODUCT_R,
        Product2Constants.ObjectFields.MATERIAL_NUMBER
    )

    product2s_target = sfdc_service_target.product2_service.get_products_by_material_numbers(product2_material_numbers)
    
    ccproduct_sku_to_product2_material_number_map = dict()
    for ccproduct in ccproducts_in_source.iterable:
        ccproduct_sku = ccproduct.get(CCProductConstants.ObjectFields.SKU)
        product2_material_number = ccproduct.get(CCProductConstants.ObjectFields.SMA_CC_PRODUCT_R).get(Product2Constants.ObjectFields.MATERIAL_NUMBER)
        ccproduct_sku_to_product2_material_number_map[ccproduct_sku] = product2_material_number

    ccproducts_in_target = sfdc_service_target.ccproduct_service.load_all(additional_fields)

    ccproducts_to_update_in_target = []
    for ccproduct_target in ccproducts_in_target:
        ccproduct_sku = ccproduct_target.get(CCProductConstants.ObjectFields.SKU)
        product2_sku_to_find = ccproduct_sku_to_product2_material_number_map.get(ccproduct_sku)

        for product2 in product2s_target.iterable:
            product2_material_number = product2.get(Product2Constants.ObjectFields.MATERIAL_NUMBER)
            
            if(product2_sku_to_find == product2_material_number):
                ccproduct_target[CCProductConstants.ObjectFields.SMA_CC_PRODUCT] = product2.get(SObjectConstants.ID)
                ccproducts_to_update_in_target.append(ccproduct_target)
                break

    ccproducts_to_update_in_target = remove_keys(ccproducts_to_update_in_target, [CCProductConstants.ObjectFields.SMA_CC_PRODUCT_R])

    update_response = sfdc_service_target.bulk_update_ccproducts(ccproducts_to_update_in_target)
 
    CCProductDmlOperationResponseHandler(update_response).handle()
    
def copy_product2(skus, sfdc_service_source:SfdcService, sfdc_service_target:SfdcService):
     #check existing data for product2
    existing_product2s_lw_target = sfdc_service_target.product2_service.get_products_by_material_numbers(skus)
    if(existing_product2s_lw_target.size() == len(skus)):
        print("all skus exist as  products2 on target org")
    else:
        print("not all exist on target, lets find out which skus")
        missing_skus_product2 = find_missing_skus_in_product_list(
            skus,
            existing_product2s_lw_target.iterable,
            Product2Constants.ObjectFields.MATERIAL_NUMBER
        )

        #load product2 from source
        missing_product2s_lw_source = sfdc_service_source.product2_service.get_products_by_material_numbers(
            missing_skus_product2,
            additional_fields=[
                Product2Constants.ObjectFields.IS_ACTIVE,
                Product2Constants.ObjectFields.DESCRIPTION,
                Product2Constants.ObjectFields.MATERIAL_NUMBER 
            ]
        )
        if(missing_product2s_lw_source.size() > 0):
            #insert into target
            missing_product2s_lw_source.iterable = remove_keys(
                missing_product2s_lw_source.iterable,
                [SObjectConstants.ID]
            )
            product2_insert_response = sfdc_service_target.bulk_insert_product2(missing_product2s_lw_source.iterable)
            Product2DmlOperationResponseHandler(product2_insert_response).handle()

def copy_cc_products(skus, sfdc_service_source:SfdcService, sfdc_service_target:SfdcService)->CCProductListWrapper:

    new_cc_products = CCProductListWrapper(
        []
    )

    #check existing data for cc product
    existing_cc_product_lw_target = sfdc_service_target.ccproduct_service.get_products_by_sku(skus)
    if(existing_cc_product_lw_target.size() == len(skus)):
        print("all skus exist as cc products on target org")
    else:
        print("not all cc products exist on target, lets find out which skus")
        missing_skus_ccproducts = find_missing_skus_in_product_list(
            skus,
            existing_cc_product_lw_target.iterable,
            CCProductConstants.ObjectFields.SKU
        )

        #load cc products from source
        cc_products_from_source_lw = sfdc_service_source.ccproduct_service.get_products_by_sku(
            missing_skus_ccproducts
        )

        if(cc_products_from_source_lw.size() > 0):
        
            product2_lw_target = sfdc_service_target.product2_service.get_products_by_material_numbers(skus, additional_fields=[
                Product2Constants.ObjectFields.MATERIAL_NUMBER
            ])

            cc_products_to_insert = remove_keys(
                cc_products_from_source_lw.iterable,
                [SObjectConstants.ID]
            )

            sfdc_service_target.link_product2s_with_ccproducts(
                cc_products_to_insert,
                product2_lw_target.iterable
            )

            cc_product_insert_response = sfdc_service_target.bulk_insert_ccproducts(cc_products_to_insert)

            CCProductDmlOperationResponseHandler(cc_product_insert_response).handle()

            new_cc_products = sfdc_service_target.ccproduct_service.get_products_by_sku(missing_skus_ccproducts)

    return new_cc_products

def copy_cc_product_media(sfdc_service_source:SfdcService, sfdc_service_target:SfdcService, new_cc_products:CCProductListWrapper):
    
    skus = new_cc_products.get_all_skus()

    cc_product_media_source_IW = sfdc_service_source.ccproduct_media_service.load_records_by_product_skus(skus)
    
    cc_product_media_record_types_in_target = sfdc_service_target.load_product_media_recordtypes()

    cc_product_medias_to_insert = set_target_product_ids(cc_product_media_source_IW.iterable, new_cc_products.iterable)
    cc_product_medias_to_insert = set_target_record_type_ids(cc_product_media_source_IW.iterable, cc_product_media_record_types_in_target)
    cc_product_medias_to_insert = remove_keys_from_cc_product_related_object(cc_product_medias_to_insert)

    SfdcDmlOperationResponseHandler(
        sfdc_service_target.bulk_insert_cc_product_media(cc_product_medias_to_insert),
        sobject=CCProductMediaConstants.SOBJECTNAME
    ).handle()

def copy_cc_product_tabs(sfdc_service_source:SfdcService, sfdc_service_target:SfdcService, new_cc_products:CCProductListWrapper):

    skus = new_cc_products.get_all_skus()

    cc_product_tabs_source_IW = sfdc_service_source.ccproduct_tab_service.load_records_by_product_skus(skus)

    cc_product_tabs_to_insert = set_target_product_ids(cc_product_tabs_source_IW.iterable, new_cc_products.iterable)
    cc_product_tabs_to_insert = remove_keys_from_cc_product_related_object(cc_product_tabs_to_insert)
    
    SfdcDmlOperationResponseHandler(
        sfdc_service_target.bulk_insert_cc_product_tabs(cc_product_tabs_to_insert),
        sobject= CCProductTabConstants.SOBJECTNAME
    ).handle()

def copy_cc_product_tabs_by_skus(skus, sfdc_service_source:SfdcService, sfdc_service_target:SfdcService):

    cc_products_target = sfdc_service_target.ccproduct_service.get_products_by_sku(skus)

    ccproduct_sku_to_id_map = dict()

    for product in cc_products_target.iterable:
        ccproduct_sku_to_id_map[product.get(CCProductConstants.ObjectFields.SKU)] = product.get(SObjectConstants.ID)

    cc_product_tabs_source = sfdc_service_source.ccproduct_tab_service.load_records_by_product_skus(skus)

    product_tabs_to_insert = []
    for product_tab in cc_product_tabs_source.iterable:
        product_id = ccproduct_sku_to_id_map.get(
            product_tab.get(CCProductTabConstants.ObjectFields.PRODUCT_R).get(CCProductConstants.ObjectFields.SKU)
        )
        if(product_id != None):
            product_tab[CCProductTabConstants.ObjectFields.PRODUCT] = product_id
            product_tabs_to_insert.append(product_tab)

    product_tabs_to_insert = remove_keys(product_tabs_to_insert, [CCProductTabConstants.ObjectFields.PRODUCT_R, SObjectConstants.ID, SObjectConstants.NAME])

    insert_response = sfdc_service_target.bulk_insert_cc_product_tabs(product_tabs_to_insert)

    SfdcDmlOperationResponseHandler(insert_response, sobject=CCProductTabConstants.SOBJECTNAME).handle()

def copy_cc_product_tabs_from_locale_to_another_locale(source_locale, target_locale, sfdc_service_target:SfdcService):

    cc_product_tabs_source = sfdc_service_target.ccproduct_tab_service.load_records_by_locale(source_locale)

    cc_product_tabs_to_update = []
    for cc_product_tab in cc_product_tabs_source.iterable:
        cc_product_tab[CCProductTabConstants.ObjectFields.LOCALE] = target_locale
        cc_product_tabs_to_update.append(cc_product_tab)

    
    cc_product_tabs_to_update = remove_keys(cc_product_tabs_to_update, [CCProductTabConstants.ObjectFields.PRODUCT_R, SObjectConstants.ID, SObjectConstants.NAME])

    update_response = sfdc_service_target.bulk_insert_cc_product_tabs(cc_product_tabs_to_update)



    SfdcDmlOperationResponseHandler(update_response, sobject=CCProductTabConstants.SOBJECTNAME).handle()


def create_cc_price_lists(new_cc_products:CCProductListWrapper, sfdc_service_target:SfdcService)->IterableWrapper:

    storefronts = IterableWrapper(
        new_cc_products.get_storefronts()
    )

    cc_price_lists_to_insert = IterableWrapper(
        []
    )

    cc_price_list_names = []

    if(storefronts.is_not_empty_and_not_null()):
        for storefront in storefronts.iterable:
            currency_iso_code = get_currency_iso_code_by_storefront(storefront)

            name = storefront + " - price list ("+currency_iso_code+")"
            cc_price_list_names.append(name)

            cc_price_lists_to_insert.append(
                {
                    CCPriceListConstants.NAME : name, 
                    CCPriceListConstants.ObjectFields.CCRZ_CURRENCYISOCODE : currency_iso_code,
                    CCPriceListConstants.CURRENCY_ISO_CODE : currency_iso_code
                }
            )
    
    cc_price_list_insert_response = sfdc_service_target.bulk_insert_cc_price_lists(
        cc_price_lists_to_insert.iterable
    )

    SfdcDmlOperationResponseHandler(
        cc_price_list_insert_response,
        sobject=CCPriceListConstants.SOBJECTNAME 
    ).handle()

    cc_price_lists = sfdc_service_target.ccprice_list_service.load_cc_price_lists_by_names(cc_price_list_names)

    return cc_price_lists

def create_cc_price_list_items(
        new_cc_products:CCProductListWrapper,
        cc_price_lists:IterableWrapper, 
        sfdc_service_target:SfdcService
    )->IterableWrapper:

    

    for product in new_cc_products.iterable:
        pass

    pass



def get_currency_iso_code_by_storefront(storefront):
    
    currency_iso_code = None

    EUR_storefronts = ['emercepower', 'SMAEmployeeShop', 'SMAag', 'SMAspareparts', 'SMAit', 'SMAfr']
    HUF_storefronts = ['emerce']
    ZAR_storefronts = ['emerceZA']

    if(storefront in EUR_storefronts):
        currency_iso_code = "EUR"
    elif(storefront in HUF_storefronts):
        currency_iso_code = "HUF"
    elif(storefront in ZAR_storefronts):
        currency_iso_code = "ZAR"
    else:
        print("Warning: no currency iso code found for", storefront)

    return currency_iso_code

def set_target_product_ids(related_object_list, product_list):
    new_related_object_list = []
    for record in related_object_list:
        cc_product_media_product_sku = record.get(CCProductRelatedConstants.ObjectFields.CCPRODUCT_R).get(CCProductConstants.ObjectFields.SKU)
        for product in product_list:
            product_sku = product.get(CCProductConstants.ObjectFields.SKU)
            if(product_sku == cc_product_media_product_sku):
                record[CCProductRelatedConstants.ObjectFields.CCPRODUCT] = product.get(SObjectConstants.ID)
                new_related_object_list.append(record)
                break
    return new_related_object_list

def set_target_record_type_ids(related_object_list, record_type_list):
    new_list = []
    for record in related_object_list:
        cc_product_media_record_type_name =  record.get(SObjectConstants.RECORDTYPE).get(SObjectConstants.NAME)
        for recordtype in record_type_list:
            if(recordtype.get(SObjectConstants.NAME) == cc_product_media_record_type_name):
                record[SObjectConstants.RECORDTYPEID] = recordtype.get(SObjectConstants.ID)
                new_list.append(record)
                break
    return new_list

def find_missing_skus_in_product_list(skus, product_list, sku_field_api_name):
    missing_skus = []
    for sku in skus:
        found_sku = False
        for product in product_list:
            if(product.get(sku_field_api_name) == sku):
                found_sku = True
                break
        if(found_sku == False):
            missing_skus.append(sku)
    return missing_skus

def remove_keys_from_cc_product_related_object(records):
    return remove_keys(
        records,
        [
            SObjectConstants.ID,
            SObjectConstants.NAME,
            CCProductMediaConstants.ObjectFields.CCPRODUCT_R,
            CCProductMediaConstants.ObjectFields.RECORDTYPE,
        ]
    )

def remove_keys(records, keys):
    new_list = []
    for record in records:
        for key in keys:
            if(key in record):
                del record[key]
        new_list.append(record)
    return new_list


Main()


