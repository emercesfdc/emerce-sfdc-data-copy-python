class KwargWrapper:

    class KwargUnwrapper:
        
        def __init__(self, a, b, c=None) -> None:
            print("a", a)
            print("b", b)
            print("c", c)

    def __init__(self, **kwargs) -> None:
        kwargUnwrapper = self.KwargUnwrapper(**kwargs)


kwargWrapper = KwargWrapper(a=1,b=2, c=3)
