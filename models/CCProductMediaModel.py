from wrappers.CCProductListWrapper import CCProductListWrapper
from wrappers.IterableWrapper import IterableWrapper
from builders.SoqlBuilder import SoqlBuilder
from models.SObjectModel import SObjectModel
from constants.CCProductConstants import CCProductConstants
from constants.CCProductMediaConstants import CCProductMediaConstants

class CCProductMediaModel(SObjectModel):

    additional_fields = [
        CCProductMediaConstants.ObjectFields.PRODUCT_MEDIA_SOURCE,
        CCProductMediaConstants.ObjectFields.RECORDTYPE_ID,
        CCProductMediaConstants.ObjectFields.RECORDTYPE_NAME, 
        CCProductMediaConstants.ObjectFields.URI,
        CCProductMediaConstants.ObjectFields.CCPRODUCT,
        CCProductMediaConstants.ObjectFields.CCPRODUCT_SKU,
        CCProductMediaConstants.ObjectFields.ENABLED,
        CCProductMediaConstants.ObjectFields.MEDIA_TYPE
    ]

    def __init__(self, sfdc_connector) -> None:
        super().__init__(
            sfdc_connector,
            CCProductMediaConstants.SOBJECTNAME
        )

    def load_records_by_product_skus(self, skus, additional_fields = None) -> IterableWrapper:
        q_add_fields = self.additional_fields

        if(not additional_fields == None):
            q_add_fields = additional_fields

        data = super().load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    CCProductMediaConstants.ObjectFields.CCPRODUCT_SKU +
                    " in {param}",
                    param=skus
                ),
                SoqlBuilder.WhereCondition(
                    " and "+CCProductMediaConstants.ObjectFields.RECORDTYPE_ID +
                    " != null"
                ),
            ], 
            additional_fields=q_add_fields
        )
        return IterableWrapper(data)
