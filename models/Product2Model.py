from wrappers.Product2ListWrapper import Product2ListWrapper
from builders.SoqlBuilder import SoqlBuilder
from constants.Product2Constants import Product2Constants
from models.SObjectModel import SObjectModel


class Product2Model(SObjectModel):

    additional_fields = [
        Product2Constants.ObjectFields.MATERIAL_NUMBER,
        Product2Constants.ObjectFields.WEIGHT_NET,
        Product2Constants.ObjectFields.GROSS_WEIGHT,
        Product2Constants.ObjectFields.WIDTH_GROSS,
        Product2Constants.ObjectFields.HEIGHT_GROSS,
        Product2Constants.ObjectFields.DEPTH_GROSS,
        Product2Constants.ObjectFields.UNIT_WEIGHT,
        Product2Constants.ObjectFields.UNIT_DIMENSION,
        Product2Constants.ObjectFields.UNIT_VOLUME,
        Product2Constants.ObjectFields.AVAILABLE_IN_SHOP,
        Product2Constants.ObjectFields.IS_ACTIVE
    ]

    def __init__(self, sfdc_connector) -> None:
        super().__init__(
            sfdc_connector,
            Product2Constants.SOBJECT_API_NAME
        )

    def load_records_by_skus(self, skus, additional_fields = None) -> Product2ListWrapper:
        data = super().load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    Product2Constants.ObjectFields.MATERIAL_NUMBER +
                    " in {param}",
                    param=skus
                )
            ],
            additional_fields=additional_fields
        )
        return Product2ListWrapper(data)
