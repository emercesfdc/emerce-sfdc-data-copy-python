from wrappers.CCProductListWrapper import CCProductListWrapper
from wrappers.IterableWrapper import IterableWrapper
from builders.SoqlBuilder import SoqlBuilder
from models.SObjectModel import SObjectModel
from constants.CCProductLocaleConstants import CCProductLocaleConstants

class CCProductLocaleModel(SObjectModel):

    additional_fields = [
        CCProductLocaleConstants.ObjectFields.LONG_DESC,
        CCProductLocaleConstants.ObjectFields.PRODUCT,
        CCProductLocaleConstants.ObjectFields.PRODUCT_SKU,
        CCProductLocaleConstants.ObjectFields.SHORT_DESC,
        CCProductLocaleConstants.ObjectFields.LOCALE   
    ]

    def __init__(self, sfdc_connector) -> None:
        super().__init__(
            sfdc_connector,
            CCProductLocaleConstants.SOBJECTNAME
        )

    def load_records_by_product_skus(self, skus, additional_fields = None) -> IterableWrapper:
        q_add_fields = self.additional_fields

        if(not additional_fields == None):
            q_add_fields = additional_fields

        data = super().load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    CCProductLocaleConstants.ObjectFields.PRODUCT_SKU +
                    " in {param}",
                    param=skus
                )
            ], 
            additional_fields=q_add_fields
        )
        return IterableWrapper(data)
