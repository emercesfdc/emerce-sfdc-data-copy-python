from builders.SoqlBuilder import SoqlBuilder
from constants.SObjectConstants import SObjectConstants


class SObjectModel:

    additional_fields = []
    
    def __init__(self, sfdc_connector, sobject) -> None:
        self.sfdc_connector = sfdc_connector
        self.base_fields = [
            SObjectConstants.ID,
            SObjectConstants.NAME
        ]
        self.sobject = sobject

    def load_records(self, where_conditions = None, additional_fields = None) -> list:
        
        add_fields = self.additional_fields
        if(not additional_fields == None):
            add_fields = additional_fields

        soql_request = SoqlBuilder.create_soql_request(
            self.sobject,
            self.base_fields + add_fields,
            where_conditions
        )

        return self.sfdc_connector.query_all(
            soql_request
        )

    def set_additional_fields(self, additional_fields):
        self.additional_fields = additional_fields
