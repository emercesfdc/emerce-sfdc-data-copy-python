from wrappers.CCProductListWrapper import CCProductListWrapper
from wrappers.IterableWrapper import IterableWrapper
from builders.SoqlBuilder import SoqlBuilder
from models.SObjectModel import SObjectModel
from constants.CCProductConstants import CCProductConstants
from constants.CCPriceListConstants import CCPriceListConstants

class CCPriceListModel(SObjectModel):

    additional_fields = [
        CCPriceListConstants.CURRENCY_ISO_CODE,
        CCPriceListConstants.ObjectFields.CCRZ_CURRENCYISOCODE
    ]

    def __init__(self, sfdc_connector) -> None:
        super().__init__(
            sfdc_connector,
            CCPriceListConstants.SOBJECTNAME
        )

    def load_records_by_names(self, names, additional_fields = None) -> IterableWrapper:
        data = super().load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    CCPriceListConstants.NAME +
                    " in {param}",
                    param=names
                )
            ], 
            additional_fields=additional_fields
        )
        return IterableWrapper(data)
