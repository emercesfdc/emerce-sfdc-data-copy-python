from wrappers.CCProductListWrapper import CCProductListWrapper
from wrappers.IterableWrapper import IterableWrapper
from builders.SoqlBuilder import SoqlBuilder
from models.SObjectModel import SObjectModel
from constants.CCProductConstants import CCProductConstants


class CCProductModel(SObjectModel):

    additional_fields = [
        CCProductConstants.ObjectFields.SKU,
        CCProductConstants.ObjectFields.PRODUCT_STATUS,
        CCProductConstants.ObjectFields.STOREFRONTS,
        CCProductConstants.ObjectFields.SHIPPING_WEIGHT,
        CCProductConstants.ObjectFields.SHORT_DESCRIPTION,
        CCProductConstants.ObjectFields.LONG_DESCRIPTION,
        CCProductConstants.ObjectFields.CLASSIFICATION
    ]

    def __init__(self, sfdc_connector) -> None:
        super().__init__(
            sfdc_connector,
            CCProductConstants.SOBJECT_API_NAME
        )

    def load_records_by_skus(self, skus, additional_fields = None) -> CCProductListWrapper:
        data = super().load_records(
            where_conditions=[
                SoqlBuilder.WhereCondition(
                    CCProductConstants.ObjectFields.SKU +
                    " in {param}",
                    param=skus
                )
            ], 
            additional_fields=additional_fields
        )
        return CCProductListWrapper(data)
