from constants.CCProductMediaConstants import CCProductMediaConstants
from utils.DictUtil import DictUtil
from simple_salesforce import Salesforce, format_soql


class SfdcConnector:

    def __init__(self, **kwargs) -> None:

        self.connection = Salesforce(
            **kwargs
        )

    def query_all(self, soql_request):

        soql_response = self.connection.query_all(
            soql_request.full_query
        )

        records = soql_response.get("records")

        return records

    def get_bulk_insert_product2_func(self):
        return self.connection.bulk.Product2.insert

    def get_bulk_insert_ccproduct_func(self):
        return self.connection.bulk.ccrz__E_Product__c.insert

    def get_bulk_update_ccproduct_func(self):
        return self.connection.bulk.ccrz__E_Product__c.update

    def get_bulk_insert_cc_product_media_func(self):
        return self.connection.bulk.ccrz__E_ProductMedia__c.insert

    def get_bulk_insert_cc_price_list_items_func(self):
        return self.connection.bulk.ccrz__E_PriceListItem__c.insert

    def get_bulk_insert_cc_product_categoriesfunc(self):
        return self.connection.bulk.ccrz__E_ProductCategory__c.insert

    def get_bulk_insert_cc_product_tab_func(self):
        return self.connection.bulk.ccrz__E_ProductTab__c.insert

    def get_bulk_insert_cc_price_list_func(self):
        return self.connection.bulk.ccrz__E_PriceList__c.insert

    def get_bulk_insert_cc_product_locale_func(self):
        return self.connection.bulk.ccrz__E_ProductItemI18N__c.insert
    

    def load_record_types_cc_product_media(self):
        return self.connection.query_all(
            format_soql(
                "SELECT Name, Id FROM RecordType where Sobjecttype = '" +
                CCProductMediaConstants.SOBJECTNAME+"'"
            )
        ).get("records")
